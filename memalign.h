#ifndef FIO_MEMALIGN_
#define FIO_MEMALIGN_H

#include "compiler/compiler.h"

BEGIN_C_DECLS

extern void *fio_memalign(size_t alignment, size_t size);
extern void fio_memfree(void *ptr, size_t size);

END_C_DECLS

#endif
