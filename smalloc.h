#ifndef FIO_SMALLOC_H
#define FIO_SMALLOC_H

#include "compiler/compiler.h"

BEGIN_C_DECLS

extern void *smalloc(unsigned int);
extern void sfree(void *);
extern char *smalloc_strdup(const char *);
extern void sinit(void);
extern void scleanup(void);

extern unsigned int smalloc_pool_size;

END_C_DECLS

#endif
